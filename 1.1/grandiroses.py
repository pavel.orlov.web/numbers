from turtle import Turtle, Screen
from math import pi, sin, cos
x = 0
y = 0
screen = Screen()
turtle = Turtle()
turtle.speed(0)
screen.setworldcoordinates(-pi, -5, pi, 4)

a = 3
turtle.speed(0)
parts = float(input("Количество лепестков:  "))
for angle in range(1000):
    rad = a*sin(parts*angle)
    turtle.penup()
    turtle.goto(x, y)
    turtle.write('⚫')
    x = rad*cos(angle)
    y = rad*sin(angle)
    turtle.speed(0)

screen.exitonclick()
