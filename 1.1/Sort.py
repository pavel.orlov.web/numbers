try:
    print("Введите название файла")
    # Сортировка вставками
    a = []
    with open(input(), 'r') as file:
        for line in file:
            line = line.rstrip('\n')
            a.append(line)

    result = [int(item) for item in a]

    def insertion_sort(massiva):
        for i in range(1, len(massiva)):
            value = massiva[i]
            j = i - 1
            while j >= 0 and value < massiva[j]:
                massiva[j + 1] = massiva[j]
                j -= 1
            massiva[j + 1] = value
        return massiva

    print("The unsorted list is:", result)
    print("The sorted list1 is:", insertion_sort(result))
    print('Хотите перезаписать / записать файл y/n? ')

    if input() == "y":
        print("Введите имя выходного файла")
        with open(input(), "w") as f:
            f.writelines(str(insertion_sort(result)))
    else:
        print("Введите имя нового файла")
        with open(input(), "w") as f:
            f.writelines(str(insertion_sort(result)))

except FileNotFoundError:
    print("Такого файла не существует, либо его название не верно")
    print("Попробуйте добавить в конце файла .txt")