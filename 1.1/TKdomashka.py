import tkinter as tk
from tkinter import messagebox

def calculate():
    value = calc.get()
    calc.delete(0, tk.END)
    try:
        calc.insert(0, eval(value))
    except NameError:
        messagebox.showinfo("Внимание!", "Нужно вводить только цифры, вы ввели другие символы")
    except ZeroDivisionError:
        messagebox.showinfo("Внимание", "ДЕЛИТЬ НА НОЛЬ НЕЛЬЗЯ!")
    except SyntaxError:
        messagebox.showinfo("Критическая ошибка", "Произошла неизветная ошибка, пожалуйста попробуйте ещё раз.")

def press_key(event):
    print(event.char)
    if event.char.isdigit():
        add_digit(event.char)
    elif event.char in "+-*/":
        add_operation(event.char)
    elif event.char in "\r":
        calculate()

def delete_button():
    value = calc.get()
    calc.delete(0, tk.END)

def make_delete_button():
    return tk.Button(win, command=delete_button, text="C", bd=3, font=("Arial", 16))

def add_digit(digit):
    value = calc.get()
    calc.delete(0,tk.END)
    calc.insert(0, value+digit)

def add_operation(operation):
    value = calc.get()
    if value[-1] in "+-*/":
        value=value[:-1]
    elif "+" in value or "-" in value or "*" in value or "/" in value:
        calculate()
        value = calc.get()
    calc.delete(0,tk.END)
    calc.insert(0, value+operation)

def make_conclusion_button(conclusion):
    return tk.Button(win, text=conclusion, command=calculate, bd=3, font=("Arial", 16))


def make_operation_button(operation):
    return tk.Button(win, text=operation, command=lambda: add_operation(operation),
                     bd=3,
                     font=("Arial", 18),
                     fg="white",
                     bg="#ff4500"
                     )

def make_digit_button(digit):
    return tk.Button(win, text=digit, command=lambda: add_digit(digit), bd=3, font=("Arial", 16))


# Определение окна
win = tk.Tk()
win.title('My first programm')
win.geometry("240x280+50+50")
win.resizable(False, False)
win.config(bg='#ACF2E9')

win.bind("<Key>", press_key)

# Основная часть
calc = tk.Entry(win, justify=tk.RIGHT, font=("Helvetica", 18), width=15)
calc.grid(row=0, column=0, columnspan=4, stick="we", pady=5, padx=2)
make_digit_button("1").grid(row=1, column=0, stick="wens", padx=2, pady=2)
make_digit_button("2").grid(row=1, column=1, stick="wens", padx=2, pady=2)
make_digit_button("3").grid(row=1, column=2, stick="wens", padx=2, pady=2)
make_digit_button("4").grid(row=2, column=0, stick="wens", padx=2, pady=2)
make_digit_button("5").grid(row=2, column=1, stick="wens", padx=2, pady=2)
make_digit_button("6").grid(row=2, column=2, stick="wens", padx=2, pady=2)
make_digit_button("7").grid(row=3, column=0, stick="wens", padx=2, pady=2)
make_digit_button("8").grid(row=3, column=1, stick="wens", padx=2, pady=2)
make_digit_button("9").grid(row=3, column=2, stick="wens", padx=2, pady=2)
make_digit_button("0").grid(row=4, column=0, stick="wens", padx=2, pady=2)

make_operation_button("+").grid(row=1, column=3, stick="wens", padx=2, pady=2)
make_operation_button("-").grid(row=2, column=3, stick="wens", padx=2, pady=2)
make_operation_button("*").grid(row=3, column=3, stick="wens", padx=2, pady=2)
make_operation_button("/").grid(row=4, column=3, stick="wens", padx=2, pady=2)

make_conclusion_button("=").grid(row=4, column=1, stick="wens", padx=2, pady=2)
make_delete_button().grid(row=4, column=2, stick="wens", padx=2, pady=2)

# Размеры кнопок
win.grid_columnconfigure(0, minsize=60)
win.grid_columnconfigure(1, minsize=60)
win.grid_columnconfigure(2, minsize=60)
win.grid_columnconfigure(3, minsize=60)

win.grid_rowconfigure(1, minsize=60)
win.grid_rowconfigure(2, minsize=60)
win.grid_rowconfigure(3, minsize=60)
win.grid_rowconfigure(4, minsize=60)

win.mainloop()
