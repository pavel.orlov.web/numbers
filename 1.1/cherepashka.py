# -*- coding: utf8 -*-
import turtle
import random

colors = ["green", "blue", "yellow", "red", "purple"]

# границы
win = turtle.Screen()
border = turtle.Turtle()
border.speed(0)
border.pensize(5)
border.hideturtle()
border.color("black")
border.penup()
border.goto(300, 300)
border.pendown()
border.goto(300, -300)
border.goto(-300, -300)
border.goto(-300, 300)
border.goto(-300, 300)
border.goto(300, 300)

# мячик
ball = turtle.Turtle()
ball.hideturtle()
ball.shape("circle")

dx = 1
dy = 1
ball.up()
ranx = random.randint(-200, 200)
rany = random.randint(-200, 200)
ball.showturtle()
ball.goto(ranx, rany)

while True:
    x, y = ball.position()
    if x+dx >= 300 or x+dx <= -300:
        dx = -dx
    if y+dy >= 300 or y+dy <= -300:
        dy = -dy

ball.goto(x+dx, y+dy)


win.mainloop()

