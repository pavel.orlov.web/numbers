import numpy as np
import matplotlib.pyplot as plt

help = []
sec = []

# txt to int
file = open("webping.txt", "r").readlines()
for i in range(len(file)-1):
    won = file[i].rstrip()
    help.append(won)

for b in range(len(help)-1):
    new = help[b]
    new = float(new)
    sec.append(new)

data = sec
plt.hist(data,edgecolor="black", bins =20)
plt.title("«Histogram for 1500 elements»")
plt.ylabel("Колличество пингов")
plt.xlabel("Пинг")
plt.show()