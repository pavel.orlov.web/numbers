mac = input("Enter the MAC in format F0-79-59-70-A7-20: ").replace("-", "")[:6]
mac = mac.upper()
database_mac = open("macinvest.txt", 'r', encoding="utf-8").readlines()
for i in range(len(database_mac)):
    if mac in database_mac[i]:
        print(database_mac[i])
        exit()
print("Sorry, we can't find a right model of your web device!")