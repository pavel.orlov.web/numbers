import mathkaif as mk

def test_len_vector():
    l1 = [3, 4]
    exp = 5
    res = mk.len_vector(l1)
    assert res == exp

def test_sum_vectors():
    l1 = [3, 4]
    l2 = [2, 5]
    exp = [5, 9]
    res = mk.sum_vectors(l1 ,l2)
    assert res == exp
    
def test_sub_vectors():
    l1 = [3, 4]
    l2 = [2, 5]
    exp = [1, -1]
    res = mk.sub_vectors(l1, l2)
    assert res == exp
    
def test_mult_vectors_on_num():
    vect = [3, 4]
    num = 2
    res = [6, 8]
    exp = mk.mult_vectors_on_num(vect, num)
    assert res == exp

def test_div_vectors():
    vect = [4, 2]
    num = 2
    exp = [2, 1]
    res = mk.div_vectors(vect, num)
    assert res == exp

def test_scalar_mult_vectors_on_num():
    l1 = [3, 4]
    l2 = [2, 5]
    exp = 26
    res = mk.scalar_mult_vectors_on_num(l1, l2)
    assert res == exp

def test_collar_vectors():
    l1 = [3, 4]
    l2 = [2, 5]
    exp = False
    res = mk.collar_vectors(l1, l2)
    assert res == exp

def test_sonaprav():
    l1 = [2, 4]
    l2 = [4, 8]
    exp = True
    res = mk.sonaprav(l1 ,l2)
    assert res == exp

def test_protivprav():
    l1 = [2, 4]
    l2 = [4, 8]
    exp = False
    res = mk.protivprav(l1, l2)
    assert res == exp
    
def test_ravenstvo():
    l1 = [3, 4]
    l2 = [2, 5]
    exp = False
    res = mk.ravenstvo(l1, l2)
    assert res == exp
    
def test_ortogon():
    l1 = [3, 4]
    l2 = [2, 5]
    exp = False
    res = mk.ortogon(l1, l2)
    assert res == exp
    
def test_normalization():
    l1 = [3, 4]
    exp = 7
    res = mk.normalization(l1)
    assert res == exp
    
def test_change_direction():
    l1 = [3, 4]
    exp = [-3, -4]
    res = mk.change_direction(l1)
    assert res == exp
    
def test_angle_between_vectors():
    l1 = [3, 4]
    l2 = [2, 5]
    exp = 0.96
    res = mk.angle_between_vectors(l1, l2)
    assert res == exp
    
def test_proection_vector_on_vector():
    l1 = [3, 4]
    l2 = [2, 5]
    exp = 4.82
    res = mk.proection_vector_on_vector(l1, l2)
    assert res == exp
    
def test_os_between_vectors():
    l1 = [3, 4]
    l2 = [2, 5]
    exp = 0.96
    res = mk.cos_between_vectors(l1, l2)
    assert res == exp