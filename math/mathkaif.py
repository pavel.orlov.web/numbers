import math

# Функция для проверки длин векторов
def check(l1, l2):
    if len(l1) != len(l2):
        raise ValueError("Wrong len of vectors")

# Длинна вектора
def len_vector(l1):
    len_l1 = 0
    for i in range(len(l1)):
        len_l1 += l1[i]**2
        a = round((len_l1**(1/2)), 2)
    result = a
    return result

# Сложение векторов
def sum_vectors(l1, l2):
    arr_sum = []
    check(l1, l2)
    for i in range(len(l1)):
        arr_sum.append(l1[i]+l2[i])
    result = arr_sum
    return result

# Вычитание векторов
def sub_vectors(l1, l2):
    arr_sub = []
    check(l1, l2)
    for i in range(len(l1)):
        arr_sub.append(l1[i]-l2[i])
    result = arr_sub
    return result

# Умножение вектора на число
def mult_vectors_on_num(vect, num):
    arr_mult = []
    l = vect
    num = num
    for i in range(len(l)):
        arr_mult.append(l[i]*num)
    result = arr_mult
    return result

# Деление вектора на число
def div_vectors(vect, num):
    result = [vect[0] / num, vect[1] / num]
    return result

# Скалярное произведение векторов
def scalar_mult_vectors_on_num(l1, l2):
    result = 0
    check(l1, l2)
    for i in range(len(l1)):
        result += l1[i]*l2[i]
    return result

# Коллинеарность векторов (ДА или НЕТ)
def collar_vectors(l1, l2):
    check(l1, l2)
    temp = []
    collar = True
    for i in range(len(l1)-1):
        if (l1[i] / l2[i]) == (l1[i+1] / l2[i + 1]):
            temp.append("YES")
        else:
            temp.append("NO")
    if "NO" in temp:
        return False
    else:
        return True

# Сонаправленность векторов
def sonaprav(l1, l2):
    cosangle = cos_between_vectors(l1, l2)
    if cosangle == 1:
        return True
    else:
        return False


# противоположнонаправленность вектора
def protivprav(l1, l2):
    cosangle = cos_between_vectors(l1, l2)
    if cosangle == -1:
        return True
    else:
        return False

# равенство векторов
def ravenstvo(l1, l2):
    check(l1, l2)
    a = sonaprav(l1, l2)
    len_l1 = 0
    len_l2 = 0
    if a == True:
        for i in range(len(l1)):
            len_l1 = len_vector(l1)
            len_l2 = len_vector(l2)
        if len_l1 == len_l2:
            return True
        else:
            return False
    else:
        return False

# Ортогональность векторов
def ortogon(l1, l2):
    check(l1, l2)
    a = scalar_mult_vectors_on_num(l1, l2)
    if a == 0:
        result = True
        return result
    else:
        result = False
        return result

# Нормализация вектора
def normalization(l1):
    normalization = 0
    for i in range(len(l1)):
        normalization += abs(l1[i])
    result = normalization
    return result

# Изменение направления вектора
def change_direction(l1):
    result = 0
    for i in range(len(l1)):
        l1[i] = -l1[i]
    result = l1
    return result

# угол между векторами
def angle_between_vectors(l1, l2):
    check(l1, l2)
    a = round(scalar_mult_vectors_on_num(l1, l2)/(len_vector(l1)*len_vector(l2)), 2)
    result = a
    return result
    
# проекция вектора на вектор
def proection_vector_on_vector(l1, l2):
    scalar = scalar_mult_vectors_on_num(l1, l2)
    l2_lenght_vector = len_vector(l2)
    result = round((scalar / l2_lenght_vector), 2)
    return result

# коснус между векторами
def cos_between_vectors(l1 ,l2):
    scalar = scalar_mult_vectors_on_num(l1, l2)
    multi_lenght_vectors = len_vector(l1) * len_vector(l2)
    result = round((scalar / multi_lenght_vectors), 2)
    return result
