import mathkaif as mk
# Функции написаны для квадратных матриц 2 x 2

# def check(mat1, mat2):
#     if len(mat1) == len(mat2):
#         if len(mat1[0]) == len(mat2[0]) and len(mat1[1]) == len(mat2[1]):
#             pass
#         else:
#             raise ValueError("Matrix not squad")
    
def plus_matrix(mat1, mat2):
    check(mat1, mat2)
    result = [[mat1[0][0] + mat2[0][0], mat1[0][1] + mat2[0][1]],
              [mat1[1][0] + mat2[1][0], mat1[1][1] + mat2[1][1]]]
    return result

def minus_matrix(mat1, mat2):
    check(mat1, mat2)
    result = [[mat1[0][0] - mat2[0][0], mat1[0][1] - mat2[0][1]],
              [mat1[1][0] - mat2[1][0], mat1[1][1] - mat2[1][1]]]
    return result
    
def transponing(mat1):
    result = [[mat1[0][0], mat1[1][0]],
              [mat1[0][1], mat1[1][1]]]
    return result
    
def mult_matrix_on_scalar(mat1, scalar):
    mat1[0] = mk.mult_vectors_on_num(mat1[0], scalar)
    mat1[1] = mk.mult_vectors_on_num(mat1[1], scalar)
    result = [mat1[0], mat1[1]]
    return result

def mult_matrix(mat1, mat2):
    # check(mat1, mat2)
    transpon_mat1 = transponing(mat1)
    transpon_mat2 = transponing(mat2)
    
    result = [[mk.scalar_mult_vectors_on_num(mat1[0], transpon_mat2[0]),
               mk.scalar_mult_vectors_on_num(mat1[0], transpon_mat2[1])],
              [mk.scalar_mult_vectors_on_num(mat1[1], transpon_mat2[0]), 
               mk.scalar_mult_vectors_on_num(mat1[1], transpon_mat2[1])]]
    
    return result

def get_string_via_index(mat1, index):
    index = index - 1
    return mat1[index]

def get_column_via_index(mat1, index):
    index = index - 1
    transpon_mat1 = transponing(mat1)
    
    return transpon_mat1[index]

def move_string(mat1):
    result = [mat1[1], mat1[0]]
    return result

def mult_strin_mat_on_scalar(mat1, index, scalar):
    index = index - 1
    result = mk.mult_vectors_on_num(mat1[index], scalar)
    return result

def plus_minus_vector_mult_on_scalar(mat1, mat2, index, scalar):
    check(mat1, mat2)
    mat1[0], mat1[1] = mk.mult_vectors_on_num(mat1[0], scalar), mk.mult_vectors_on_num(mat1[1], scalar)
    mat2[0], mat2[1] = mk.mult_vectors_on_num(mat2[0], scalar), mk.mult_vectors_on_num(mat2[1], scalar)
    res_str = plus_matrix(mat1, mat2), minus_matrix(mat1, mat2)
    result = [res_str[0][index], res_str[1][index]]
    
    return result