import find_back_matrix as bm

def test_find_back_matrix():
    matrix = [[- (1 / 6), 1 / 3], [1 / 3, - (1 / 6)]]
    exp = [[2, 4], [4, 2]]
    res = bm.find_back_matrix(matrix)
    assert res == exp
    
def test_slau_via_back_matrix():
    slau_A = [[2, 1], [3, -1]]
    slau_B = [[11], [9]]
    exp = [[4.0], [3.0]]
    res = bm.slau_via_back_matrix(slau_A, slau_B)
    assert res == exp