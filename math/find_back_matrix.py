import mathkaif as mk
import matrix as m
# Поиск ообратной матрицы методом Гаусса Жордана

def find_back_matrix(mat):
    onemat = [[1, 0], [0, 1]]

    # домножается верний вектр до нижнего что бы сделать вычитание из нижнего
    div = mat[1][0] / mat[0][0]
    mat[0] = mk.mult_vectors_on_num(mat[0], div)
    onemat[0] = mk.mult_vectors_on_num(onemat[0], div)
    
    # вычитание верхнего вектора из нижнего
    mat[1] = mk.sub_vectors(mat[1], mat[0])
    onemat[1] = mk.sub_vectors(onemat[1], onemat[0])
    
    if mat[1][1] > mat[0][1]:
        # домножается нижний вектр до верхнего что бы сделать вычитание из верхнего
        div = mat[0][1] / mat[1][1]
        mat[1] = mk.mult_vectors_on_num(mat[1], div)
        onemat[1] = mk.mult_vectors_on_num(onemat[1], div)

        
    if mat[1][1] < mat[0][1]:
        # домножается нижний вектр до верхнего что бы сделать вычитание из верхнего
        div = mat[0][1] / mat[1][1]
        mat[1] = mk.mult_vectors_on_num(mat[1], div)
        onemat[1] = mk.mult_vectors_on_num(onemat[1], div)

    
    # вычитание нижнего вектора из верхнего
    mat[0] = mk.sub_vectors(mat[0], mat[1])
    onemat[0] = mk.sub_vectors(onemat[0], onemat[1])

    
    # у нас получается пример 
    #|x 0|
    #|0 y|
    # делим первый вектор на x а второй на y что бы придти к виду 
    # |1 0|
    # |0 1|

    onemat[0], onemat[1] = mk.div_vectors(onemat[0], mat[0][0]), mk.div_vectors(onemat[1], mat[1][1])
    mat[0], mat[1] = mk.div_vectors(mat[0], mat[0][0]), mk.div_vectors(mat[1], mat[1][1])
    for i in range(len(onemat)):
        for k in range(len(onemat[i])):
            if type(onemat[i][k]) == int:
                onemat[i][k] = int(onemat[i][k])
            else:
                onemat[i][k] = round(onemat[i][k], 1)
                
    return onemat

def slau_via_back_matrix(slau_mat, res_slau_mat): # res_slau_mat can be like [[9], [11]]
    A = slau_mat
    B = res_slau_mat
    B[0].append(1)
    B[1].append(1)
    X = m.mult_matrix(find_back_matrix(A), B)
    res_x1 = [round(X[0][0], 2)]
    res_x2 = [round(X[1][0], 2)]
    result = [res_x1, res_x2]
    return result

