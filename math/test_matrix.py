import matrix as m

def test_plus_matrix():
    mat1 = [[1, 2], [3, 4]]
    mat2 = [[5, 6], [7, 8]]
    exp = [[6, 8], [10, 12]]
    res = m.plus_matrix(mat1, mat2)
    assert res == exp
    
def test_minus_matrix():
    mat1 = [[1, 2], [3, 4]]
    mat2 = [[5, 6], [7, 8]]
    exp = [[-4, -4], [-4, -4]]
    res = m.minus_matrix(mat1, mat2)
    assert res == exp

def test_transponing():
    mat1 = [[1, 2], [3, 4]]
    exp = [[1, 3], [2, 4]]
    res = m.transponing(mat1)
    assert res == exp

def test_mult_matrix_on_scalar():
    mat1 = [[1, 2], [3, 4]]
    scalar = 2
    exp = [[2, 4], [6, 8]]
    res = m.mult_matrix_on_scalar(mat1, scalar)
    assert res == exp
    
def test_mult_matrix():
    mat1 = [[1, 2], [3, 4]]
    mat2 = [[5, 6], [7, 8]]
    exp = [[19, 22], [43, 50]]
    res = m.mult_matrix(mat1, mat2)
    assert res == exp
    
def test_get_string_via_index():
    mat1 = [[1, 2], [3, 4]]
    index = 1
    exp = [1, 2]
    res = m.get_string_via_index(mat1, 1)
    assert res == exp
    
def test_get_column_via_index():
    mat1 = [[1, 2], [3, 4]]
    index = 1
    exp = [1, 3]
    res = m.get_column_via_index(mat1, 1)
    assert res == exp
    
def test_move_string():
    mat1 = [[1, 2], [3, 4]]
    exp = [[3, 4], [1, 2]]
    res = m.move_string(mat1)
    assert res == exp
    
def test_mult_strin_mat_on_scalar():
    mat1 = [[1, 2], [3, 4]]
    index = 1
    scalar = 2
    exp = [2, 4]
    res = m.mult_strin_mat_on_scalar(mat1, 1, 2)
    assert res == exp
    
def test_plus_minus_vector_mult_on_scalar():
    mat1 = [[1, 2], [3, 4]]
    mat2 = [[5, 6], [7, 8]]
    index = 1
    scalar = 2
    exp = [[20, 24], [-8, -8]]
    res = m.plus_minus_vector_mult_on_scalar(mat1, mat2, index, scalar)
    assert res == exp