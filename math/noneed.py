import mathkaif as mk
onece_matrix = [[1, 0], [0, 1]]
def find_back_matrix(onece_matrix):
    
    if onece_matrix[1][0] > onece_matrix[0][0]:
        div = (onece_matrix[1][0] / onece_matrix[0][0])
        onece_matrix[0][0] = int(onece_matrix[0][0] * div)
        onece_matrix[0][1] = int(onece_matrix[0][1] * div)
        onece_matrix[1] = mk.sub_vectors(onece_matrix[1], onece_matrix[0])
        if onece_matrix[0][1] > onece_matrix[1][1]:
            onece_matrix[1][1] = int(onece_matrix[1][1] * (onece_matrix[0][1] / onece_matrix[1][1]))
            onece_matrix[1][0] = int(onece_matrix[1][0] * (onece_matrix[1][1] / onece_matrix[0][1]))
            onece_matrix[0][1] -= onece_matrix[1][1]
            onece_matrix[0][0] -= onece_matrix[1][0]
            
            onece_matrix[0] = mk.div_vectors(onece_matrix[0], onece_matrix[0][0])
            onece_matrix[1] = mk.div_vectors(onece_matrix[1], onece_matrix[1][1])
    return onece_matrix
print(find_back_matrix([[1, 0], [0, 1]]))