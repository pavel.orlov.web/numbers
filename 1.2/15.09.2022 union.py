# it"s just a little thing to enjoy the MILKY WAY
import pygame

#craate screen
pygame.init()
size = [600, 600]
screen = pygame.display.set_mode(size, False)
clock = pygame.time.Clock()

class Planet:
    def __init__(self, cordX, cordY, radius, color):
        self.cordX = cordX
        self.cordY = cordY
        self.radius = radius
        self.color = color
    
    def drawplanet(self):
        pygame.draw.circle(screen, self.color,
            (self.cordX, self.cordY), self.radius)

    def rotate(self):
        if self.cordX < 600:
            self.cordX += 2
        else:
            running = False

#main
running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

#drawing and flip
    screen.fill("black")
    earth = Planet(360, 300, 5, "green")
    sun = Planet(300, 300, 25, "yellow")
    mercury = Planet(335, 300, 2, "orange")
    venus = Planet(345, 300, 5, "orange")
    mars = Planet(375, 300, 5, "green")
    jupiter = Planet(410, 300, 25, "blue")
    saturn = Planet(465, 300, 15, "white")
    uranus = Planet(510, 300, 20, "brown")
    neptune = Planet(560, 300, 20, "blue")
    sun.rotate()
    planet = [sun, mercury, venus, earth, mars, jupiter, uranus, neptune, saturn]

    for i in range(len(planet)):
        planet[i].drawplanet()

    pygame.display.flip()
    clock.tick(30)

pygame.quit()
