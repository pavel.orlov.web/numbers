from email import message
from email.mime import image
import tkinter
from tkinter import messagebox
import pygame
import random
from tkinter import *
pygame.init()



#screen size
dis_width = 600
dis_height = 400

clock = pygame.time.Clock()
 
snake_block = 10
snake_speed = 15

def start():
    global screen
    screen = pygame.display.set_mode((dis_width, dis_height))

def our_snake(snake_block, snake_list):
    for x in snake_list:
        pygame.draw.rect(screen, [255, 125, 233], [x[0], x[1], snake_block, snake_block])

def gameLoop():
    start()
    game_over = False
    game_close = False
    x1 = dis_width / 4
    y1 = dis_height / 4
    x1_change = 0
    y1_change = 0
    count_pause = 2
    pause = False
    score = 0
    num_dir = "none"
    snake_List = []
    lenght = 1
    food_x = round(random.randrange(0, dis_width - snake_block) / 10) * 10
    food_y = round(random.randrange(0, dis_height - snake_block) / 10) * 10
    # MUSIC 
    pygame.mixer.set_num_channels(10)
    pygame.mixer.music.load('sounds/snakebg.mp3')
    pygame.mixer.music.set_volume(0.3)
    pygame.mixer.music.play(-1)
    s = pygame.mixer.Sound("sounds/eatfood.mp3")
    d = pygame.mixer.Sound("sounds/snakegameover.mp3")
 
    while not game_over and pause != True:
        while game_close == True:
            screen.fill([255,255,255])
            pygame.display.update()
 
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                game_over = True

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT and num_dir !="right":
                    x1_change, y1_change = -snake_block, 0
                    num_dir = "left"
                elif event.key == pygame.K_RIGHT and num_dir != "left":
                    x1_change, y1_change = snake_block, 0
                    num_dir = "right"
                elif event.key == pygame.K_UP and num_dir != "down":
                    y1_change, x1_change = -snake_block, 0
                    num_dir = "up"
                elif event.key == pygame.K_DOWN and num_dir != "up":
                    y1_change, x1_change = snake_block, 0
                    num_dir = "down"
                elif event.key == pygame.K_ESCAPE:
                    count_pause += 1
                    if count_pause % 2 == 0:
                        pause = False
                    if count_pause % 2 == 1:
                        pause = True
                    print(pause)
 
        if x1 >= dis_width or x1 < 0 or y1 >= dis_height or y1 < 0:
            d.play()
            pygame.mixer.music.stop()
            helper = ["THATS GREATE!!!!! YOUR SCORE IS ", score]
            tkinter.messagebox.showinfo("FINALLY :(((((((((((", helper)
            pygame.quit()

        x1 += x1_change
        y1 += y1_change
        screen.fill([255,255,255])

        pygame.draw.rect(screen, [255,0,0], [food_x, food_y, snake_block, snake_block])
        snake_Head = []
        snake_Head.append(x1)
        snake_Head.append(y1)
        snake_List.append(snake_Head)

        if len(snake_List) > lenght:
            del snake_List[0]
        for x in snake_List[:-1]:
            if x == snake_Head:
                d.play()
                pygame.mixer.music.stop()
                helper = ["THATS GREATE!!!!! YOUR SCORE IS ", score]
                tkinter.messagebox.showinfo("FINALLY :(((((((((((", helper)
                pygame.quit()
                
        our_snake(snake_block, snake_List)
 
        pygame.display.update()
 
        if x1 == food_x and y1 == food_y:
            food_x = round(random.randrange(0, dis_width - snake_block) / 10) * 10
            food_y = round(random.randrange(0, dis_height - snake_block) / 10) * 10
            s.play()
            lenght += 1
            score += 1
            print(score)
 
        clock.tick(10 + lenght)

 # >>>>>>>>>>>>>>>>>>>>>>>>>> TKINTER BLOCK >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
 # GAME MENU
root = Tk()
root.title("Snake by Pavel Or.LOVE")
root.geometry("1000x400")
root.resizable(height=False, width=False)

def button_play_func():
    root.destroy()
    gameLoop()
def button_exit_func():
    root.destroy()
def button_readme_func():
    pass

# Add image file
bg = PhotoImage(file = "images/backgroundsnake.png")
button_play = PhotoImage(file = "images/playbutton.png")
button_exit = PhotoImage(file = "images/exitbutton.png")
button_readme = PhotoImage(file = "images/readme.png")

# Create Canvas
canvas1 = Canvas( root, width = 600, height = 400)

#BUTTONS#
but1 = Button(root, text = 'Click Me !', image = button_play, command=button_play_func)
but1.place(x=200, y=100)

but2 = Button(root, text="sd", image= button_exit, command=button_exit_func)
but2.place(x=200, y=195)

but3 = Button(root, text="sd", image= button_readme, state="active")
but3.place(x=500, y=195)

canvas1.pack(fill = "both", expand = True)
canvas1.create_image( 0, 0, image = bg, anchor = "nw")

root.mainloop()
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<