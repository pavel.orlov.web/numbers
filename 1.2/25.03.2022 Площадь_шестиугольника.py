import math
try:
    class hexagon():
        __radious = 0
        __area = 0

        def __init__(self, radious):
            self.__radious = radious
            self.calc_area()

        def set_x(self, new_radious):
            self.__radious = new_radious
            self.calc_area()

        def calc_area(self):
            self.__area = (((self.__radious**2) * math.sqrt(3))/4)*6

        def radious(self):
            return self.__radious

        def area(self):
            return self.__area

        print("Введите радиус окружности, для вычисления площади 6-угольника")

    r = hexagon(int(input()))



    print("radious: ", r.radious())

    print("area: ", r.area())

except:
    print("Введите целое число: ")

