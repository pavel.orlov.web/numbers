import sys
import pygame
from pygame.locals import *
import random

colors = [(0, 0, 255), (0, 255, 0), (255, 0, 0)]


FPS = 10
silver = (194, 194, 194)
black = (0, 0, 0)
pygame.init()
screen = pygame.display.set_mode((600, 400), RESIZABLE, 32)
clock = pygame.time.Clock()
font = pygame.font.SysFont("Helvetica", 35, bold=True, italic=False)
year = 2022

while True:
    clock.tick(FPS)
    for i in pygame.event.get():
        if i.type == QUIT:
            pygame.quit()
            sys.exit()
        elif i.type == KEYDOWN:
            print(i.key)
    year = year + 1
    text = font.render("Привет Мир! " + str(year) + " год!", True, random.choice(colors))
    screen.fill(random.choice(colors))
    screen.blit(text, [150, 170])
    pygame.display.update()