import random
import pygame
from tkinter import *
import tkinter.messagebox as mb

background = pygame.image.load("C:\Users\Professional\Desktop\numbers\1.2\sounds\backgroumd.jpg")
backgroundRect = (0, 0, 800, 800)
def Collision(kx, ky, ball, obj):
    if kx > 0:
        dx = ball.right - obj.left
    else:
        dx = obj.right - ball.left
    if ky > 0:
        dy = ball.bottom - obj.top
    else:
        dy = obj.bottom - ball.top

    if (dx - dy) < 5:
        kx, ky = -kx, -ky
    if dx > dy:
        ky *= -1
    if dy > dx:
        kx *= -1

    return kx, ky


pygame.init()
pygame.mixer.music.load("sounds/background.mp3")
pygame.mixer.music.play(-1)
pygame.mixer.music.set_volume(0.2)
def showresults(var):
    if var == "loose":
        root = Tk()
        root.title("Результат")
        root.resizable(width=False, height=False)
        root.geometry("300x150")
        field = Label(root, text="ВЫ ПРОИГРАЛИ((, БЛИН!", font=("Times New Roman", 18, "bold"))
        field.pack()
        root.mainloop()
    if var == "win":
        root = Tk()
        root.title("Результат")
        root.resizable(width=False, height=False)
        root.geometry("300x150")
        field = Label(root, text="ВЫ ВЫЙГРАЛИ, УРА!", font=("Times New Roman", 18, "bold"))
        field.pack()
        root.mainloop()

score = 0
W = 700
H = 700
FPS = 60
running = True
radius = 12
win, gameover = False, False


#Коэфиценты скорости
coef = [1.2, 1.3, 1.4, 1.6, 2]
kx = random.choice(coef)
ky = random.choice(coef)


scr = pygame.display.set_mode((W, H))

brickBlocks = [pygame.Rect(87 * i, 85 + 87 * j, 85, 85) for i in range(8) for j in range(3)]
colorBlocks = [pygame.Color(255, 117, 24) for i in range(8) for j in range(3)]
player = pygame.Rect(225, 675, 150, 10)
ball = pygame.Rect(294, 650, radius, radius)
ballMoveing = True
ballThrowing = False

while running:
    scr.blit(background, (0, 0))

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.MOUSEBUTTONDOWN and gameover == False and win == False:
            ballMoveing = False
            ballThrowing = True
    key = pygame.key.get_pressed()
    if key[pygame.K_d] and player.right < W-2 and gameover == False and win == False:
        player.x +=3
        if ballMoveing == True:
            ball.x +=1
    if key[pygame.K_a] and player.left > 0+2 and gameover == False and win == False:
        player.x +=-3
        if ballMoveing == True:
            ball.x +=-1

    if ballThrowing == True:
        ball.x += 1 * kx
        ball.y += 1 * ky

        if ball.x < radius or ball.x > W - radius:
            kx *= -1
        if ball.y - 75 < radius:
            ky *= -1

        if ball.colliderect(player) and ball.y > 0:
            kx, ky = Collision(kx, ky, ball, player)

        destr = ball.collidelist(brickBlocks)
        if destr != -1:
            destr_2 = brickBlocks.pop(destr)
            kx, ky = Collision(kx, ky, ball, destr_2)
            score +=1

    pygame.draw.line(scr, pygame.Color("White"), [0, 75], [W, 75], 3)
    [pygame.draw.rect(scr, colorBlocks[color], i) for color, i in enumerate(brickBlocks)]
    pygame.draw.rect(scr, pygame.Color("White"), player)
    pygame.draw.circle(scr, pygame.Color("Red"), ball.center, radius)
    if ball.y > H:
        ballThrowing, running = False, True
        player.x, player.y =225, 675
        ball.x, ball.y =294, 650
        pygame.mixer.music.stop()
        pygame.mixer.Sound("sounds/gameover.wav").play().set_volume(0.3)
        gameover = True
        running = False
        showresults("loose")

    if score == 24:
        ballThrowing, running = False, True
        player.x, player.y =225, 675
        ball.x, ball.y =294, 650
        pygame.mixer.music.stop()
        pygame.mixer.Sound("sounds/win.wav").play().set_volume(0.3)
        running = False
        showresults("win")


    pygame.display.flip()
