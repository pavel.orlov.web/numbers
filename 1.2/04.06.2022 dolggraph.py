import math as m

ms={i[0]:list(map(int,i.split()[1:])) for i in open('C:\\Users\\msm.txt').readlines()[1:]}
l = [print(i, " ".join(map(str, (ms[i])))) for i in ms]

inp_correct = False
while not inp_correct:
    src = input('Which startpoint?' + str([i for i in ms.keys()]) + ': ')
    dest = input('Which destpoint?' + str([i for i in ms.keys()]) + ': ')
    inp_correct = src in ms.keys() and dest in ms.keys()
    if not inp_correct:
        print('This is out of graph!')

lengths, paths = {}, {}
visited = []
for i in ms.keys():
    lengths[i] = m.inf
paths[src] = []
lengths[src] = 0

while dest not in visited:
    l = m.inf
    minnode = ''
    for n in filter(lambda x: x not in visited, ms.keys()):
        if l > lengths[n]:
            minnode = n
            l = lengths[n]
    visited.append(minnode)
    for i in range(len(ms[minnode])):
        if ms[minnode][i] == 0:
            continue
        curnode = list(ms.keys())[i]
        if ms[minnode][i] + lengths[minnode] < lengths[curnode]: 
            lengths[curnode] = ms[minnode][i] + lengths[minnode]
            paths[curnode] = paths[minnode] + [minnode]

print("Path from", src, "to", dest, "is", "-".join(paths[dest] + [dest]), "with a min path", lengths[minnode])