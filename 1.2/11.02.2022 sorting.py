try:
    inputFile = open('numbers.txt', 'r', encoding='utf-8')
    list1 = inputFile.read()
    nums = list1.split()

    s = len(nums)**2

    for b in range(len(nums)-1):
        nums[b] = int(nums[b])
        nums[-1] = int(nums[-1])

    def main():
        for i in range(len(nums)-1):
            if nums[i] > nums[i+1]:
                nums[i], nums[i+1] = nums[i+1], nums[i]

    def right(time, fun):
        for m in range(time): fun()
        print(nums)

    right(s, main)
except ValueError:
    print("Пожалуйста, проверьте что бы в файле были записаны только числовые значения через пробел")