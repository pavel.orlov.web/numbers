def fin(grp):
    filename = "groups.txt"
    massive: list = []
    with open(filename, "r") as file:
        for i in range(5):
            line = file.readline().split()
            massive.append(line)

    for b in range(5):
        for c in range(5):
            massive[b][c] = int(massive[b][c])

    ep = 10

    def search_groups(ar, row, elem):
            if ar[row][elem] == grp:
                ar[row][elem] = ep
                if elem < len(ar[row]) - 1:
                    search_groups(ar, row, elem + 1)
                if row < len(ar)-1:
                    search_groups(ar, row + 1, elem)
                if elem > 0:
                    search_groups(ar, row, elem - 1)
                if row > 0:
                    search_groups(ar, row - 1, elem)
            else:
                return
            
    rec_a = [i.copy() for i in massive]
    gr_cntr = 0

    for row in range(len(rec_a)):
        for elem in range(len(rec_a)):
            if rec_a[row][elem] == grp:
                gr_cntr += 1
                search_groups(rec_a, row, elem)
    print("groups with", grp, ":", gr_cntr)


fin(1)
fin(2)
fin(3)
fin(4)
fin(5)
fin(6)
fin(7)
fin(8)
fin(9)


