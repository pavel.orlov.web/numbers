# Task 1
def task1():
    a = 1
    b = 2
    c = 3
    t = 0
    print(id(a), id(b), id(c), id(t))

    b = t
    b = c
    c = a
    a = t
    print(id(a), id(b), id(c), id(t))


# Task 2.1
def task2_1():
    try:
        num1 = int(input("Enter first number: "))
        num2 = int(input("Enter second number: "))
        result = num1 + num2
        print("summary of", num1, "and", num2, "is:", result)
    except ValueError:
        print("Please enter a num. Example: 12")
        task2_1()
        
        
# Task 2.2
def task2_2():
    result = 0
    try:
        n = int(input("Введите желаемое колличество чисел для сложения: "))
        for i in range(n):
            result += int(input("Enter the num: "))
        print(result)
    except ValueError:
        print("Please enter a num. Example: 12")
        task2_2()
    

# Task 3.1
def task3_1():
    try:
        x = int(input("Enter x, where 0 <= x <= 100: "))
        if x >= 0 and x <= 100:
            print(x, "^5 is: ", x**5, sep="")
        else:
            print("Please enter a num >= 0 and <= 100. Example: 54")
            task3_1()
    except ValueError:
        print("Please enter a num >= 0 and <= 100. Example: 54")
        task3_1()
      

# Task 3.2
def task3_2():
    try:
        vertex = 5
        x = int(input("Enter x, where 0 <= x <= 100: "))
        result = 1
        if x >= 0 and x <= 100:
            for i in range(vertex):
                result *= x
        else:
            print("Please enter a num >= 0 and <= 100. Example: 54")
        print(x, "^5 is: ", result, sep="")
    except ValueError:
        print("Please enter a num >= 0 and <= 100. Example: 54")
        task3_2()


# Task 4
def task4():
    fibonacci = [0, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233]
    try:
        num1 = int(input("Enter a one num from 0 to 255. Example: 144: "))
        if num1 > 0 and num1 < 250:
            if num1 in fibonacci:
                print("Число", num1, "является числом Фибоначчи")
            else:
                print("Число", num1, "не является числом Фибоначчи")
        else:
            print("Please enter a num >= 0 and <= 250. Example: 144")
    except ValueError:
        print("Please enter a num >= 0 and <= 250. Example: 144")
        task4()


# Task 5 ПЕРВЫЙ СПОПСБ
def task5_1():
    mounths = {
        "Декабрь" : 1,
        "Январь" : 2,
        "Февраль" : 3,
        "Март" : 4,
        "Апрель" : 5,
        "Май" : 6,
        "Июнь" : 7,
        "Июль" : 8,
        "Август" : 9,
        "Сентябрь" : 10,
        "Октябрь" : 11,
        "Ноябрь" : 12,
    }
    input_mounts = str(input("Введите месяц. Например: Январь \n"))
    if input_mounts in mounths:
        key_mounth = mounths[input_mounts]
        if key_mounth >= 1 and key_mounth <= 3:
            print("Зима")
        elif key_mounth >= 4 and key_mounth <= 6:
            print("Весна")
        elif key_mounth >= 7 and key_mounth <= 9:
            print("Лето")
        elif key_mounth >= 10 and key_mounth <= 12:
            print("Осень")
    else:
        print("Проверьте корректоность ввода месяца")


# Task 5 ВТОРОЙ СПОСОБ
def task5_2():
    season = {
        "Декабрь" : "Зима",
        "Январь" : "Зима",
        "Февраль" : "Зима",
        "Март" : "Весна",
        "Апрель" : "Весна",
        "Май" : "Весна",
        "Июнь" : "Лето",
        "Июль" : "Лето",
        "Август" : "Лето",
        "Сентябрь" : "Осень",
        "Октябрь" : "Осень",
        "Ноябрь" : "Осень",
    }
    input_mounth = str(input("Введите название месяца. Например: Январь \n"))
    if input_mounth not in season:
        print("Проверьте корректность ввода месяца.")
    print(season.get(input_mounth))


# Task 6
def task6():
    sumOdd = 0
    sumEven = 0
    try:
        rangeNums = int(input("Введите верхнюю границу, тоесть от 1 до x. Например: 12 "))
        countEven = rangeNums // 2
        countOdd = rangeNums - countEven
        for i in range(rangeNums+1):
            if i % 2 == 0:
                sumEven += i
            elif i % 2 == 1:
                sumOdd += i
        print(sumEven, sumOdd, countEven, countOdd)
    except ValueError:
        print("Введите целочисленное число. Например: 12")
        task6()


# Task 7
def task7():
    counter = 0
    try:
        rangeNums = int(input("Введите верхнюю границу, тоесть от 1 до x. Например: 12 "))
        for i in range(1, rangeNums+1):
            for j in range(1, rangeNums+1):
                if i % j == 0:
                    counter += 1
            print(i, counter, sep=" : ")
            counter = 0
    except ValueError:
        print("Введите целочисленное число. Например: 12")
        task7()


# Task 8
def task8():
    try:
        bottomNum = int(input("Введите число нижней границы. Например 1: "))
        topNum = int(input("Введите число верхней границы. Например 3: "))
        if topNum > bottomNum:
            for i in range(bottomNum, topNum):
                ii = i**2
                for j in range(bottomNum, topNum):
                    jj = j**2
                    for k in range(bottomNum, topNum):
                        kk = k**2
                        if ii + jj == kk:
                            print(i, j, k)
        else:
            print("Проверьте еорректность введённой границы")
            print("Нижняя граница должна быть строго меньше верхней")
    except ValueError:
        print("Проверьте еорректность введённой границы ")
        task8()

# Task 9
def task9():
    devList = []
    try:
        bottomNum = int(input("Введите число нижней границы. Например 1: "))
        topNum = int(input("Введите число верхней границы. Например 3: "))
        if topNum > bottomNum:
            for i in range(bottomNum, topNum):
                i_list = list(map(int, str(i)))
                for j in range(1, i+1):
                    if i % j == 0:
                        devList.append(j)
                if set(i_list).issubset(devList) == True:
                    print(i)
                    devList = []
        else:
            print("Проверьте еорректность введённой границы")
            print("Нижняя граница должна быть строго меньше верхней")
    except ValueError:
        print("Проверьте еорректность введённой границы ")
        task9()
task9()

# Task 10
def task10():
    # Массив делителей
    devList = []
    bottomNum = 5 # нижняя граница
    