import re
import itertools

class Supportive:
      """class - support"""
      
      def __init__(self, file_name : str):
            self.__file_name = file_name
            self.__ourline = str
            self.__current_dots_lst = []
            self.__num_of_vertex = 3
            
            self.__create_current_lst()
            self.create_combinations_list()
            
      def __open_file(self):
            self.file = open(self.__file_name, "r", encoding="UTF-8").readlines()
      
      def __create_temp_lst(self):
            self.__ourline = self.file[0]
            self.__num = re.findall(r'\d+', self.__ourline)
      
      def __create_current_lst(self):
            self.__open_file()
            self.__create_temp_lst()
            self.__check_lst()
            
            for i in range(len(self.__num)-1):
                  if i % 2 == 0:
                        temp_lst = [int(self.__num[i]), int(self.__num[i+1])]
                        self.__current_dots_lst.append(temp_lst)
      
      # checking nums of pairs, must be even becouse dot has 2 coords
      def __check_lst(self):
            if len(self.__num) % 2 != 0:
                  raise ValueError("List must be even")
      
      def create_combinations_list(self):
            self.perm_set = itertools.combinations(self.__current_dots_lst, self.__num_of_vertex)
      
      def get_current_lst(self):
            return self.__current_dots_lst
      
      def get_current_combination_list(self):
            return self.perm_set
      
      def __eq__(self, other):
            pass
            
      
class Triangle:
      """Triangle"""
      """every vertex dot of triangle is a vectror (list) ex: [2, 5]"""
      
      def __init__(self, vector_list : list):
            """init triangles dots"""
            
            # 3 vertex of triangle
            self.vector_list = vector_list
            self.dot_1 = self.vector_list[0]
            self.dot_2 = self.vector_list[1]
            self.dot_3 = self.vector_list[2]
            
      def compute_area(self):
            return 1/2 * abs(self.dot_1[0]*(self.dot_2[1] - self.dot_3[1])+
                              self.dot_2[0]*(self.dot_3[1] - self.dot_1[1])+
                              self.dot_3[0]*(self.dot_1[1] - self.dot_2[1])) 
      
a = Supportive("plist.txt")

area_lst = []

for i in a.get_current_combination_list():
      b = Triangle(i)
      area = b.compute_area()
      if area != 0:
            area_lst.append(area)
      
print(max(area_lst), min(area_lst))

      