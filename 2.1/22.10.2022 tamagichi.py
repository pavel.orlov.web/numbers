import sys
import time
from os import system
import keyboard

system("pip3 install keyboard")

counter_time = 0

switcher_condition = {
    1: "I`m dead",
    2: "Please, do somthing!!!",
    3: "I guess I can stay alive about few second",
    4: "Pretty good, owner, love ya!",
    5: "PERFECTO, THE LIFE IS SO GOOD, BRUH!!!! <3"
}

class Animal():
    def __init__(self, name, health = 100, condition = 5, happiness = 100, food_size = 20, play_size = 20, food = 100, time = 0, gameover = False):
        self.name = str(name)
        self.food_size = int(food_size)
        self.food = int(food)
        self.play_size = int(play_size)
        self.health = int(health)
        self.condition = int(condition)
        self.time = int(time)
        self.happiness = int(happiness)
        self.gameover = gameover
        
    def death(self):
        print("You loose!")
        sys.exit()
                
    def feed(self): 
        if self.food > 0 and self.food <= 100 - self.food_size:
            self.food += 20
        if self.food > 100 - self.food_size:
            self.food += 100 - self.food
        else:
            Animal.health_reduce(self)
    
    def play(self):
        if self.happiness > 0 and self.happiness <= 100 - self.play_size:
            self.happiness += self.play_size
        if self.happiness > 100 - self.play_size:
            self.happiness += 100 - self.play_size
        else:
            Animal.health_reduce(self)
            
    def food_reduce(self):
        if self.food > 0:   
            self.food += -self.food_size
            if self.food <= 0:
                Animal.health_reduce(self)
        else:
            self.food = 0
            
    def health_reduce (self):
        if self.food >= 0 and self.happiness <= 0:
            self.health += -self.food_size / 4
        if self.food <= 0 and self.happiness >= 0:
            self.health += -self.food_size / 4
        if self.food <= 0 and self.happiness <= 0:
            self.health += -self.food_size / 2
    
    def happiness_reduce(self):
        if self.happiness > 0:
            self.happiness += -self.play_size
            if self.happiness <= 0:
                Animal.health_reduce(self)
            else:
                self.happiness == 0
    
    def condition_reduce(self):
        if 80 < self.health <= 100:
            self.condition = 5
        if 60 < self.health <= 80:
            self.condition = 4
        if 40 < self.health <= 60:
            self.condition = 3
        if 20 < self.health <= 40:
            self.condition = 2
        if 0 < self.health <= 20:
            self.condition = 1
    
    def get_status(self):
        print("Animal name: ", self.name)
        print("<------------------------------------>")
        print("Status:")
        print("    Health: ", self.health)
        print("    Food: ", self.food)
        print("    Happiness: ", self.happiness)
        print("    Conditions: ", switcher_condition[self.condition])
        print("<------------------------------------>")
        print("Press F and ENTER to FEED")
        print("Press P and ENTER to PLAY")
        
    def get_health(self):
        return self.health
    
    def set_health(self, value):
        self.value = value
        self.health = self.value
    
    def press_feed(self):
        if self.food >= 0 and self.food <= 100:
            if keyboard.is_pressed('f'):
                Animal.feed(self)
                print("FEED!!!!!!")
        else:
            self.food == 0
    
    def press_play(self):
        if self.happiness >= 0 and self.happiness <= 100:
            if keyboard.is_pressed('p'):
                Animal.play(self)
                print("PLAY!!!!!!")
        else:
            self.happiness == 0
        
            
tamagocci = Animal("Mishka")
while True:
    
    tamagocci.press_feed()
    tamagocci.press_play()
    
    print(tamagocci.get_status())

    if counter_time == 5:
        tamagocci.get_health()
        tamagocci.food_reduce()
        tamagocci.happiness_reduce()
        tamagocci.health_reduce()
        tamagocci.condition_reduce()

        counter_time = 0
        
        get_health_status = tamagocci.get_health()
    
        if get_health_status < 0:
            
            system("cls")
            tamagocci.set_health(0)
            print(tamagocci.get_status())
            tamagocci.death()
        
    time.sleep(0.7)
    counter_time += 1
    
    system("cls")
