# it"s just a little thing to enjoy the MILKY WAY
from math import cos, sin
import sys
import pygame

W = 800
H = 600
earth_speed = 0.03
earth_size = 5
X_center = W / 2
Y_center = H / 2

"""coefficients"""
speed = {"mercury" : earth_speed / 0.241,
         "venus" : earth_speed / 0.615,
         "eaarth" : earth_speed,
         "mars" : earth_speed / 0.881,
         "jupiter" : earth_speed / 11.862,
         "saturn" : earth_speed / 29.458,
         "uranus" :earth_speed / 54.015,
         "neptune" :earth_speed / 164.788}

class Planet:
    """Planet"""
    
    def __init__(self, cordX, cordY, radius, outradious, color, angle, speed):
        self.__cordX = cordX
        self.__cordY = cordY
        self.__radius = radius
        self.__color = color
        self.__angle = angle
        self.__speed = speed
        self.__outradious = outradious
    
    def rotate(self):
        """rotate"""
        self.__cordX = W // 2 + self.__outradious * cos(self.__angle)
        self.__cordY = H // 2 + self.__outradious * sin(self.__angle)
        self.__angle -= self.__speed
        self.drawplanet()
        
    def drawplanet(self):
        pygame.draw.circle(screen, self.__color, (self.__cordX, self.__cordY), self.__radius)

sun = Planet(X_center, Y_center, 25,  0,  "yellow", 1, 1)
mercury = Planet(335, 300, 2, -40,  "orange", 1, speed["mercury"])
venus = Planet(345, 300, 5, 55,  "orange", 1, speed["venus"])
earth = Planet(360, 300, 5, -75,  "green", 1, speed["eaarth"])
mars = Planet(375, 300, 5, -91,  "brown", 1, speed["mars"])
jupiter = Planet(310, 300, 25, 125,  "blue", 1, speed["jupiter"])
saturn = Planet(365, 300, 15, -170,  "white", 1, speed["saturn"])
uranus = Planet(310, 300, 20, 210, "brown", 1, speed["uranus"])
neptune = Planet(360, 300, 20, -260,  "blue", 1, speed["neptune"])

planet = [sun, mercury, venus, earth, mars, jupiter, uranus, neptune, saturn]

"""Инициализация pygame"""
pygame.init()
screen_size = [W, H]
screen = pygame.display.set_mode(screen_size, False)
clock = pygame.time.Clock()

#main
while True:
    """main"""
    
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()

    pygame.display.flip()
    screen.fill("black")
    
    for i in range(len(planet)):
        planet[i].rotate()
        
    clock.tick(60)

pygame.quit()
